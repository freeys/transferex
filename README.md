# transferex

#### 本人后端开发，代码写的很渣，有些地方都没看懂，依葫芦画瓢，bug方面...发现的都改了...仅做抛转引玉之用

#### 介绍
基于layui 2.5.5<br/>
图标来自于 iconfont

![输入图片说明](https://images.gitee.com/uploads/images/2019/1217/154617_7d928117_1510229.png "屏幕截图.png")


在原有基础上增加了一些功能：
1. 可对选中的数据进行上下排序
2. 可动态添加，删除元素
3. 增加删除事件通知


```
    //添加数据
    transfer.addData = function (data, id) {
        var that = thisModule.that[id];
        return that.addData(data);
    }

    //删除数据
    transfer.remove = function (value, id) {
        var that = thisModule.that[id];
        return that.remove(value);
    }

    //获取data数据
    transfer.getAllData = function (id) {
        var that = thisModule.that[id];
        return that.config.data;
    }

```

```
//删除事件
onRemove: function (data, index) {
    
}
```


```
 //默认配置
    Class.prototype.config = {
        title: ['列表一', '列表二']
        , width: 200
        , height: 360
        , data: [] //数据源
        , value: [] //选中的数据
        , showSearch: false //是否开启搜索
        , id: '' //唯一索引，默认自增 index
        , text: {
            none: '无数据'
            , searchNone: '无匹配数据'
        },
        remove: false //是否显示删除
        , onRemove: undefined //删除事件
    };
```


